package com.modern.entitymanagement.network

class ApiHelper(private val apiService: ApiService) {

    suspend fun getHomeContent() = apiService.homeContent()

    suspend fun addUserData(fName: String, lName: String, email: String, phone: String?) = apiService.addUserData(fName, lName, email, phone)

    suspend  fun updateUserData(userId: Int, fname: String, lname: String, email: String, telephone: String) = apiService.updateUserData(userId,fname,lname, email, telephone)

    suspend fun deleteUser(id: Int) = apiService.deleteUser(id)

}