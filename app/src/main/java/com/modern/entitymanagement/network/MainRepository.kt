package com.modern.entitymanagement.network

import com.modern.entitymanagement.network.ApiHelper

class MainRepository(private val apiHelper: ApiHelper) {

    suspend fun getHomeContent() = apiHelper.getHomeContent()
    suspend fun addUserData(
        fName: String,
        lName: String,
        email: String,
        phone: String?
    ) = apiHelper.addUserData(fName,lName, email, phone)

    suspend fun updateUserData(userId: Int, fname: String, lname: String, email: String, telephone: String) = apiHelper.updateUserData(userId,fname,lname, email, telephone)

    suspend fun deleteUser(id: Int) = apiHelper.deleteUser(id)


}