package com.modern.entitymanagement.network

import com.modern.entitymanagement.model.MainlistResponse
import com.modern.entitymanagement.model.MainlistResponseItem
import com.modern.entitymanagement.model.commonResponse
import retrofit2.http.*

interface ApiService {

/*    @FormUrlEncoded
    @POST("Saga/Home")
    suspend fun homeContent(
        @Field("stype") type: String
    ): HomeResponseModel*/

    @GET("list")
    suspend fun homeContent(): List<MainlistResponseItem>

    @FormUrlEncoded
    @POST("store")
    suspend fun addUserData(
        @Field("fname") fName: String,
        @Field("lname") lName: String,
        @Field("email") email: String,
        @Field("telephone") phone: String?): commonResponse

    @FormUrlEncoded
    @POST("update/{id}")
    suspend fun updateUserData(
        @Path("id") id:Int,
        @Field("fname") fName: String,
        @Field("lname") lName: String,
        @Field("email") email: String,
        @Field("telephone") phone: String?): commonResponse


    @GET("delete/{id}")
    suspend fun deleteUser(@Path("id") id:Int,): commonResponse

}