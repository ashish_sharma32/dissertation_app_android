package com.modern.entitymanagement.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}