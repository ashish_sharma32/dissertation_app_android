package com.modern.entitymanagement.utils

object Constant {

    const val TYPE ="type"
    const val USERDATA = "userData"
    const val webUrl = "http://ec2-35-179-12-182.eu-west-2.compute.amazonaws.com/api/entities/"

}