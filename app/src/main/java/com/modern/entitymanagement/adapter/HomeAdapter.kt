package com.modern.entitymanagement.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.modern.entitymanagement.R
import com.modern.entitymanagement.databinding.RowTvItemBinding
import com.modern.entitymanagement.model.MainlistResponseItem

class HomeAdapter() : RecyclerView.Adapter<HomeAdapter.RadioViewHolder>() {

    var list: List<MainlistResponseItem> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RadioViewHolder {
        val binding: RowTvItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_tv_item, parent, false
        )
        return RadioViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RadioViewHolder, position: Int) {
        holder.binding.data = list[position]

    }

    fun setItemList(it: List<MainlistResponseItem>) {
        list = it
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class RadioViewHolder(itemView: RowTvItemBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
    }

    interface onClick {
        fun onRadioClick(data: List<MainlistResponseItem>, position: Int)
    }
}