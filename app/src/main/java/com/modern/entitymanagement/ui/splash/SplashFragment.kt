package com.modern.entitymanagement.ui.splash

import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.modern.entitymanagement.R
import com.modern.entitymanagement.databinding.FragmentSplashBinding


class SplashFragment : Fragment() {

    var timer: CountDownTimer? = null
    lateinit var binding: FragmentSplashBinding
    internal var view: View? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (view != null) {
            return view
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        view = binding.root
        initView()
        return view
    }

    private fun initView() {
        timer = object : CountDownTimer(3000, 1000) {

            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                findNavController().navigate(
                    R.id.mainFragment, null, NavOptions.Builder()
                        .setPopUpTo(
                            R.id.splashFragment,
                            true,
                        ).build()
                )
            }
        }

        timer?.start()
    }

    override fun onResume() {
        super.onResume()
        if (timer != null) {
            timer?.start()
        }
    }


    override fun onDetach() {
        super.onDetach()
        timer?.cancel()
    }
}