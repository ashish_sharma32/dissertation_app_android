package com.modern.entitymanagement.ui.main

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.modern.entitymanagement.MainActivity
import com.modern.entitymanagement.R
import com.modern.entitymanagement.adapter.HomeAdapter
import com.modern.entitymanagement.databinding.MainFragmentBinding
import com.modern.entitymanagement.model.MainlistResponseItem
import com.modern.entitymanagement.network.ApiHelper
import com.modern.entitymanagement.network.RetrofitBuilder
import com.modern.entitymanagement.network.Status
import com.modern.entitymanagement.network.ViewModelFactory
import com.modern.entitymanagement.utils.Constant
import com.modern.entitymanagement.utils.RecyclerItemClickListenr


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    lateinit var binding: MainFragmentBinding
    internal var view:View? = null
    lateinit var adapter: HomeAdapter
    var dataList: MutableList<MainlistResponseItem> = mutableListOf()
    var deletePosition = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (view != null) {
            return view
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)
        view = binding.root
        initView()
        return view

    }

    lateinit var activity: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (getActivity() != null) {
            activity = getActivity() as MainActivity
        }
    }

    private fun initView() {
        initBinders()
        initViewModel()
        initObservers()
    }

    private fun initObservers() {
        viewModel.userResponseData.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    activity.showDialog()
                }

                Status.SUCCESS -> {
                    activity.hideDialog()
                    it.data?.let {
                        it1 ->
                        dataList = it1 as MutableList<MainlistResponseItem>
                        adapter.setItemList(it1)
                    }
                }

                Status.ERROR -> {
                    activity.hideDialog()
                }
            }
        })

        viewModel.userDeleteResponse.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    activity.showDialog()
                }

                Status.SUCCESS -> {
                    activity.hideDialog()
                    if (deletePosition!=-1){
                        dataList.removeAt(deletePosition)
                        deletePosition = -1
                        adapter.setItemList(dataList)
                    }
                }

                Status.ERROR -> {
                    activity.hideDialog()
                }
            }
        })

        viewModel.getHomeData()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel!=null){
            viewModel.getHomeData()
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(MainViewModel::class.java)
    }

    private fun initBinders() {
        adapter = HomeAdapter()
        binding.homeListRV.adapter = adapter
        binding.homeListRV.layoutManager = LinearLayoutManager(context)

        binding.floatingActionButton.setOnClickListener {
           /* var bundle = Bundle()
            bundle.putString(Constant.TYPE, "add")
            findNavController().navigate(R.id.addEditFragment, bundle)*/

            val direction = MainFragmentDirections.mainFragmentToAddEditFragment(
                "Add User",
                "add",
                MainlistResponseItem("as","asc","AS",0,"as","sa","as")
            )
            findNavController().navigate(direction)
        }

        binding.homeListRV.addOnItemTouchListener(RecyclerItemClickListenr(context!!, binding.homeListRV, object : RecyclerItemClickListenr.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                /*var bundle = Bundle()
                bundle.putParcelable(Constant.USERDATA, dataList[position])
                bundle.putString(Constant.TYPE, "update")
                findNavController().navigate(R.id.addEditFragment, bundle)*/
                val direction = MainFragmentDirections.mainFragmentToAddEditFragment(
                    "Update User",
                    "update",
                    dataList[position]
                )
                findNavController().navigate(direction)
            }
            override fun onItemLongClick(view: View?, position: Int) {
                showDialog(position)
            }
        }))

    }

    private fun showDialog(position: Int) {
        AlertDialog.Builder(context)
            .setTitle("Delete entry")
            .setMessage("Are you sure you want to delete this entry?") // Specifying a listener allows you to take an action before dismissing the dialog.
            .setPositiveButton(android.R.string.yes,
                DialogInterface.OnClickListener { dialog, which ->
                    viewModel.deleteUser(dataList[position].id)
                    deletePosition = position
                    dialog.dismiss()
                })
            .setNegativeButton(android.R.string.no, DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }


}