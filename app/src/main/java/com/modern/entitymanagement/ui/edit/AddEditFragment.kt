package com.modern.entitymanagement.ui.edit

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.modern.entitymanagement.MainActivity
import com.modern.entitymanagement.R
import com.modern.entitymanagement.databinding.AddEditFragmentBinding
import com.modern.entitymanagement.model.MainlistResponseItem
import com.modern.entitymanagement.network.ApiHelper
import com.modern.entitymanagement.network.RetrofitBuilder
import com.modern.entitymanagement.network.Status
import com.modern.entitymanagement.network.ViewModelFactory
import com.modern.entitymanagement.ui.main.MainViewModel
import com.modern.entitymanagement.utils.Constant

class AddEditFragment : Fragment() {

    companion object {
        fun newInstance() = AddEditFragment()
    }

    private lateinit var viewModel: AddEditViewModel
    lateinit var binding: AddEditFragmentBinding
    internal var view: View?=null
    var type: String = ""
    var userId:Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (view != null) {
            return view
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.add_edit_fragment, container, false)
        view = binding.root
        initView()
        return view
    }

    private fun initView() {
        initBinder()
        initViewModel()
        initObserver()
    }

    lateinit var activity: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (getActivity() != null) {
            activity = getActivity() as MainActivity
        }
    }

    private fun initObserver() {
        viewModel.userResponseData.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    activity.showDialog()
                }

                Status.SUCCESS -> {
                    activity.hideDialog()
                    //if (it.data!!.message.equals("Success")){
                        Toast.makeText(context, it.data!!.message, Toast.LENGTH_LONG).show()
                        findNavController().navigateUp()
                //}
                }

                Status.ERROR -> {
                    activity.hideDialog()
                }
            }
        })
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(AddEditViewModel::class.java)
    }

    private fun initBinder() {
        type = arguments?.getString(Constant.TYPE).toString()
        if (type=="update"){
            val userData = arguments?.getParcelable<MainlistResponseItem>(Constant.USERDATA)
            if (userData != null) {
                userId = userData.id
                binding.userFirstNameTV.setText(userData.fname)
                binding.userLastNameTv.setText(userData.lname)
                binding.userEmailTV.setText(userData.email)
                binding.userTelephoneTV.setText(userData.telephone)
                binding.btnReport.text = "Update User"
            }
        }

        binding.btnReport.setOnClickListener {
            checkData()
        }
    }

    private fun checkData() {
        if (binding.userFirstNameTV.text.isNullOrEmpty()){
            binding.userFirstNameTV.error = "First name cannot be empty"
            return
        }else{
            binding.userFirstNameTV.error = null
        }
        if (binding.userLastNameTv.text.isNullOrEmpty()){
            binding.userLastNameTv.error = "Last name cannot be empty"
            return
        }else{
            binding.userLastNameTv.error = null
        }
        if (binding.userEmailTV.text.isNullOrEmpty()){
            binding.userEmailTV.error = "Email cannot be empty"
            return
        }else{
            binding.userEmailTV.error = null
        }
        if (binding.userTelephoneTV.text.isNullOrEmpty()){
            binding.userTelephoneTV.error = "First name cannot be empty"
            return
        }else{
            binding.userTelephoneTV.error = null
        }

        if (type=="add"){
            viewModel.addUserdata(
                binding.userFirstNameTV.text.toString().trim(),
                binding.userLastNameTv.text.toString().trim(),
                binding.userEmailTV.text.toString().trim(),
                binding.userTelephoneTV.text.toString().trim(),
            )
        }else{
            viewModel.updateUserdata(
                userId,
                binding.userFirstNameTV.text.toString().trim(),
                binding.userLastNameTv.text.toString().trim(),
                binding.userEmailTV.text.toString().trim(),
                binding.userTelephoneTV.text.toString().trim(),
            )
        }

    }

}