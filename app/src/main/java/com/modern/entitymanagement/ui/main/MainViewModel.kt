package com.modern.entitymanagement.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.modern.entitymanagement.model.MainlistResponse
import com.modern.entitymanagement.model.MainlistResponseItem
import com.modern.entitymanagement.model.commonResponse
import com.modern.entitymanagement.network.MainRepository
import com.modern.entitymanagement.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(val mainRepository: MainRepository) : ViewModel() {

    var userResponseData: MutableLiveData<Resource<List<MainlistResponseItem>>> = MutableLiveData()
    var userDeleteResponse: MutableLiveData<Resource<commonResponse>> = MutableLiveData()

    fun getHomeData() {
        viewModelScope.launch(Dispatchers.Main) {
            userResponseData.value = Resource.loading(data = null)
            try {
                userResponseData.value =
                    Resource.success(data = mainRepository.getHomeContent())
            } catch (e: Exception) {
                userResponseData.value =
                    e.localizedMessage?.let { Resource.error(data = null, message = it) }
            }
        }
    }

    fun deleteUser(id: Int) {
        viewModelScope.launch(Dispatchers.Main) {
            userDeleteResponse.value = Resource.loading(data = null)
            try {
                userDeleteResponse.value =
                    Resource.success(data = mainRepository.deleteUser(id))
            } catch (e: Exception) {
                userDeleteResponse.value =
                    e.localizedMessage?.let { Resource.error(data = null, message = it) }
            }
        }
    }

}