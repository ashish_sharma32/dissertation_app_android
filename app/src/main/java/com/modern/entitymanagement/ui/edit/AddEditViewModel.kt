package com.modern.entitymanagement.ui.edit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.modern.entitymanagement.model.MainlistResponseItem
import com.modern.entitymanagement.model.commonResponse
import com.modern.entitymanagement.network.MainRepository
import com.modern.entitymanagement.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddEditViewModel(val mainRepository: MainRepository) : ViewModel() {

    var userResponseData: MutableLiveData<Resource<commonResponse>> = MutableLiveData()

    fun addUserdata(fname: String, lname: String, email: String, telephone: String) {
        viewModelScope.launch(Dispatchers.Main) {
            userResponseData.value = Resource.loading(data = null)
            try {
                userResponseData.value = Resource.success(data = mainRepository.addUserData(fname, lname, email, telephone))
            } catch (e: Exception) {
                userResponseData.value =
                    e.localizedMessage?.let { Resource.error(data = null, message = it) }
            }
        }
    }

    fun updateUserdata(userId: Int, fname: String, lname: String, email: String, telephone: String) {
        viewModelScope.launch(Dispatchers.Main) {
            userResponseData.value = Resource.loading(data = null)
            try {
                userResponseData.value = Resource.success(data = mainRepository.updateUserData(userId, fname, lname, email, telephone))
            } catch (e: Exception) {
                userResponseData.value =
                    e.localizedMessage?.let { Resource.error(data = null, message = it) }
            }
        }
    }

}