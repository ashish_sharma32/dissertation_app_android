package com.modern.entitymanagement.model


import com.google.gson.annotations.SerializedName

data class commonResponse(
    @SerializedName("message")
    val message: String
)