package com.modern.entitymanagement.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MainlistResponseItem(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("fname")
    val fname: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("lname")
    val lname: String,
    @SerializedName("telephone")
    val telephone: String,
    @SerializedName("updated_at")
    val updatedAt: String
): Parcelable