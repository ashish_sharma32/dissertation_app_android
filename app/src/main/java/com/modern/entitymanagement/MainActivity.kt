package com.modern.entitymanagement

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.modern.entitymanagement.customDialog.ProgressDialogue
import com.modern.entitymanagement.databinding.MainActivityBinding
import com.modern.entitymanagement.ui.main.MainFragment

class MainActivity : AppCompatActivity() {

    lateinit var binding: MainActivityBinding
    lateinit var navController: NavController
    private var currentFragment: Int = 0
    lateinit var dialog: ProgressDialogue

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController

        setSupportActionBar(binding.toolbar)

        dialog = ProgressDialogue(this)
        dialog.setCancelable(false)
        val appBarConfiguration = AppBarConfiguration.Builder(
            setOf(
                R.id.mainFragment,
            )
        )
            //.setOpenableLayout(binding.drawerLayout)
            .build()
        NavigationUI.setupWithNavController(binding.toolbar, navController, appBarConfiguration)

    }

    private val listener = NavController.OnDestinationChangedListener { controller, destination, arguments ->
        // react on change
        // you can check destination.id or destination.label and act based on that
        currentFragment = destination.id
        when (destination.id) {
           R.id.splashFragment->{
                binding.appBarLayout.visibility = View.GONE
           }
            else -> {
                binding.appBarLayout.visibility = View.VISIBLE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(listener)

    }

    override fun onPause() {
        navController.removeOnDestinationChangedListener(listener)
        super.onPause()
    }


    fun showDialog() {
        if (!dialog.isShowing){
            dialog.show()
        }
    }

    fun hideDialog() {
        if (dialog.isShowing){
            dialog.cancel()
        }
    }
}